---
title: "Analysis of Overall Performance by Gender and Interval Performance"
author: "Kay Song"
date: "10/15/2019"
output: html_document
---

```{r setup, include=FALSE, echo=FALSE}
knitr::opts_chunk$set(echo = FALSE, include = FALSE)
knitr::opts_chunk$set(fig.width=10, fig.height=6) 
```

```{r basicfcn, include=F}
# This function will allow any user to install the required packages if they are already not installed.
loadPkg = function(x) { if (!require(x,character.only=T, quietly =T)) { install.packages(x,dep=T,repos="http://cran.us.r-project.org"); if(!require(x,character.only=T)) stop("Package not found") } }

loadPkg('lubridate') # used from time conversions
loadPkg('dplyr') # varios data transfers
loadPkg('ggplot2') # plotting and mapping
loadPkg('usmap') # making chloropleths
loadPkg("stats")
loadPkg("car") #vif
loadPkg("corrplot")
```

## SMART Questions

* Do men and women perform differently? Are they different in age? Are these differences statistically significant?

* Is there relationship between performace at half time (minutes it take to get to half) and official time (time it take to finish the race)?

* Does your performace at each quarter of the race (rank at each quarter, only looking at time it take to run that quarter) impact your final rank?

#### Do men and women perform differently? Are their age different?
```{r, echo=FALSE, include=FALSE}
bm_2017 <- read.csv('marathon_results_2017.csv')
str(bm_2017)
```


```{r convering time in X:XX:XX format to minutes (in number)}
bm_2017$Official.Time <- as.character(bm_2017$Official.Time) # convert to charachter, the expected input for lubridate
bm_2017$Official.Time.Min <- period_to_seconds(hms(bm_2017$Official.Time))/60 # divide by 60 to get actual minutes ran
```

```{r Some descriptive Data}
#Gender
gender.table <- table(bm_2017$M.F)
gender.freq <- as.data.frame(gender.table)
female.freq <- subset(gender.freq, Var1=="F")
male.freq <- subset(gender.freq, Var1=="M")

#Official Time by Gender
group_by(bm_2017, M.F) %>%
  summarise(
    count = n(),
    mean = mean(Official.Time.Min, na.rm = TRUE),
    sd = sd(Official.Time.Min, na.rm = TRUE)
  )

```
```{r Boxplot gender*official time, include=T}
boxplot(Official.Time.Min ~ M.F, data=bm_2017, 
        ylab="Official Time in Minutes", xlab="Gender",
        main="Official Time by Gender", col=c("#00AFBB", "#E7B800"))

boxplot(Age ~ M.F, data=bm_2017, 
        ylab="Age (years)", xlab="Gender",
        main="Age by Gender", col=c("light pink", "light green"))
```

* There are 11972 female and 14438 male participants in the data frame.

```{r T-test for age by gender}
bm_2017<- na.omit(bm_2017)
bm_2017$M.F <- as.factor(bm_2017$M.F)
OTGenderAge.ttest <- t.test(Age ~ M.F, data = bm_2017)
OTGenderAge.ttest
```
* Women's age is siginificantly different from men's age (CI[`r round(OTGenderAge.ttest$estimate, 2)`], P<0.001).

```{r T-test for average running time by gender}
bm_2017<- na.omit(bm_2017)
bm_2017$M.F <- as.factor(bm_2017$M.F)
OTGender.ttest <- t.test(Official.Time.Min ~ M.F, data = bm_2017)
OTGender.ttest
```
* Women's average oficial time is siginificantly different from men's average official time (CI[`r round(OTGender.ttest$estimate, 2)`], P<0.001).

```{r T-test for average half time by gender}
#Half time variable converted to minutes in number
bm_2017$Half <- as.character(bm_2017$Half)
bm_2017$Half.Min <- period_to_seconds(hms(bm_2017$Half))/60

HTGender.ttest <- t.test(Half.Min ~ M.F, data = bm_2017)
HTGender.ttest
names(HTGender.ttest)
```
* Women's average half time is siginificantly different from men's average half time (CI[`r round(HTGender.ttest$estimate, 2)`], p<0.001).

#### Can half time predict official time?
```{r half time correlation to official time - simple linear model}
model.OfficialHalf <- lm(Official.Time.Min ~ Half.Min, data=bm_2017)
summary(model.OfficialHalf)
coef(model.OfficialHalf)
confint(model.OfficialHalf)
```
* In a model exploring half time's relationship with official time, slope/intercept value is -3.04 and is significant (p<.001). One minute increase in Half Time positively impacts (+2.18) Official Time (p<0.001). Adjusted R squared is high at 0.894.

#### Can age predict official time?
```{r Age correlation to official time}
model.OfficialAge <- lm(Official.Time.Min ~ Age, data=bm_2017)
summary(model.OfficialAge)
coef(model.OfficialAge)
confint(model.OfficialAge)
```
* In a model exploring the participants age in relationship with official time, slope/intercept value is 202.66 and is significant (p<.001). One year increase in age increases 0.8313 minute Official Time (p<0.001). Adjusted R squared is low at 0.0507.

#### Do ranks during each quarter of the race predict overall rank? Which quarter is the best predictor for the overall rank?
```{r Ranks at each interval (in 4 quarters)}
#creating intervals of time
bm_2017$X10K <- as.character(bm_2017$X10K)
bm_2017$X10K.Min <- period_to_seconds(hms(bm_2017$X10K))/60
bm_2017$X20K <- as.character(bm_2017$X20K)
bm_2017$X20K.Min <- period_to_seconds(hms(bm_2017$X20K))/60
bm_2017$X30K <- as.character(bm_2017$X30K)
bm_2017$X30K.Min <- period_to_seconds(hms(bm_2017$X30K))/60
bm_2017$X40K <- as.character(bm_2017$X40K)
bm_2017$X40K.Min <- period_to_seconds(hms(bm_2017$X40K))/60 #Did not end up using this one

bm_2017$X0.10K.Min <- bm_2017$X10K.Min #time between 0-10K
bm_2017$X10.20K.Min <- bm_2017$X20K.Min-bm_2017$X10K.Min #time between 10-20K
bm_2017$X20.30K.Min <- bm_2017$X30K.Min-bm_2017$X20K.Min #time between 20-30K
bm_2017$X30.42K.Min <- bm_2017$Official.Time.Min-bm_2017$X30K.Min #time between 30-42.195K (end)

bm_2017$X0.10K.Rank <- rank(bm_2017$X0.10K.Min) #rank during first quarter 0-10K
bm_2017$X10.20K.Rank <- rank(bm_2017$X10.20K.Min) #rank during second quarter 10-20K
bm_2017$X20.30K.Rank <- rank(bm_2017$X20.30K.Min) #rank during third quarter 20-30K
bm_2017$X30.42K.Rank <- rank(bm_2017$X30.42K.Min) #rank during last quarter 30-42.195K

#Cor matrix and plot
str(bm_2017)
Ranks <- c("Overall", "X0.10K.Rank", "X10.20K.Rank", "X20.30K.Rank", "X30.42K.Rank")
bm_2017.Ranks <- bm_2017[Ranks]

RanksCor = cor(bm_2017.Ranks) # creating correlation matrix between all ranks.
```

```{r Ranks Cor matrix and plots, include=T}
corrplot(RanksCor, method = "number")
```

```{r Overall rank ~ ranks at each interval}
model.RankInterval <- lm(Overall ~ X0.10K.Rank+X10.20K.Rank+X20.30K.Rank+X30.42K.Rank, data=bm_2017)
summary(model.RankInterval)
vif(model.RankInterval)
```
* In a model exploring relationship between Rank at various intervals of race (0-10K, 10-20K, 20-30K, 30-42.195K) and Overall Rank, slope/intercept value is -.0686 and is significant (p<.001). 
* One rank increase during the first quarter of the race (0-10K) impacts (+0.186) overall rank (p<0.001). One rank increase during the second quarter of the race (10-20K) increases (+0.175) overall rank (p<0.001). One rank increase during the third quarter of the race (20-30K) increases (+0.291) overall rank (p<0.001). And the final quarter seems to have the highest impact on the overall rank; one rank increase during the last quarter of the race (30-40K) increases (+0.400) overall rank (p<0.001). 
* Adjusted R squared is high at 0.99.
There is concerning amount of collinearity with rank between 10-20K (vif=23.77) and rank between 20-30K (vif=17.98). 