---
title: "splitPCA"
output: html_document
---

```{r setup, include=FALSE, echo=FALSE}
knitr::opts_chunk$set(echo = FALSE, include = FALSE)
#knitr::opts_chunk$set(fig.width=10, fig.height=6) 
```

```{r}
loadPkg = function(x) { if (!require(x,character.only=T, quietly =T)) { install.packages(x,dep=T,repos="http://cran.us.r-project.org"); if(!require(x,character.only=T)) stop("Package not found") } }
loadPkg('lubridate') # used from time conversions
library(lubridate)
loadPkg('dplyr') # varios data transfers
library(dplyr)
loadPkg('ggplot2') # plotting and mapping
library(ggplot2)
loadPkg("modelr") # building linear models
library(modelr)
loadPkg("faraway") # for calculating VIF
library(faraway)
loadPkg("caret")
library(caret)
loadPkg("relaimpo")
library(relaimpo)
loadPkg("pls")
```

```{r, include=FALSE}
bm_2017 <- read.csv('marathon_results_2017.csv')
head(bm_2017)
str(bm_2017)
bm_2017$M.F <- as.factor(bm_2017$M.F)
splits <- subset(bm_2017, select = c(Age, M.F, X5K, X10K, X15K, X20K, Half, X25K, X30K, X35K, X40K, Official.Time))
head(splits)
str(splits)
levels(splits$M.F) <- c(0,1)
sum(is.na(splits))
colSums(splits == '' | splits == '-')
col_names <- colnames(splits)
for(col in col_names){
  splits <- splits[!(is.na(splits[col]) | splits[col] == '' | splits[col] == '-'), ] 
}
colSums(splits == '' | splits == '-')
```

```{r TimeConversionFunction, include=FALSE}
splits$X5K <- as.character(splits$X5K)
splits$X5K <- period_to_seconds(hms(splits$X5K))/60
splits$X10K <- as.character(splits$X10K)
splits$X10K <- period_to_seconds(hms(splits$X10K))/60
splits$X15K <- as.character(splits$X15K)
splits$X15K <- period_to_seconds(hms(splits$X15K))/60
splits$X20K <- as.character(splits$X20K)
splits$X20K <- period_to_seconds(hms(splits$X20K))/60
splits$Half <- as.character(splits$Half)
splits$Half <- period_to_seconds(hms(splits$Half))/60
splits$X25K <- as.character(splits$X25K)
splits$X25K <- period_to_seconds(hms(splits$X25K))/60
splits$X30K <- as.character(splits$X30K)
splits$X30K <- period_to_seconds(hms(splits$X30K))/60
splits$X35K <- as.character(splits$X35K)
splits$X35K <- period_to_seconds(hms(splits$X35K))/60
splits$X40K <- as.character(splits$X40K)
splits$X40K <- period_to_seconds(hms(splits$X40K))/60
splits$Official.Time <- as.character(splits$Official.Time)
splits$Official.Time <- period_to_seconds(hms(splits$Official.Time))/60
str(splits)
sum(is.na(splits))
```
## Linear Regression 1

Let us predict the Official time of the marathon by using all the predictor variables in the data.

```{r lm1, include = TRUE}
model1 <- lm(Official.Time ~ Age + M.F + X5K + X10K + X15K + X20K + Half + X25K + X30K + X35K + X40K, data = splits)
summary(model1)
vif(model1)
plot(calc.relimp(model1, type = c("lmg"), rela = TRUE))
```
In the model predicting the Official time of the marathon using Age, Gender and all the split times, we can see that all the coefficients are not significant. Only the intercept, Age and some of the split times are significant. 
R2(Adjusted) is .99 which is almost close to 1. This is because we have used all the split times till 40K. So, the model is explaining 99% of the variability in the Official time using all the split times, Gender, Age. 
But, the main problem is with the multicollinearity in the data. If we look at the vif values of the split times, they are more than 10 and this suggests that there is lot of multicollinearity in the data. We will overcome this by using PCA, PCR.
Before that, let's just predict the Official time of the marathon using Age, Gender and Half time of the marathon.

## Linear Regression 2

```{r lm2, include = TRUE}
model2 <- lm(Official.Time ~ Age + M.F + Half, data = splits)
summary(model2)
vif(model2)
plot(calc.relimp(model2, type = c("lmg"), rela = TRUE))
```
In this model, we predict the Official time of the marathon with Age, Gender and Half time of the marathon. All the coefficients are statistically significant. R2(Adjusted) is .8993. This means it is explaining 89% of the variability in Official Times with Age, Gender and Half time. And also, VIF values are 1.15, 1.22, 1.23 which indicates that there is no multicollinearity.

## PCA and PCR

Let us see whether we can apply Principal Component Analysis and Regression to our split times.

```{r PCAanalysis, include=FALSE}
split_scaled = data.frame(scale(splits[,3:11]))

pr.out = prcomp(split_scaled , scale = TRUE)

summary(pr.out)
pr.out$rotation
```

```{r biplot, include=TRUE}
biplot(pr.out, scale = 0, sub='Centered Data')
```
Principal Component analysis of the split times suggest that 1st Principal Component i.e PC1 covers 97% of the variance in the data. PC1 and PC2 cover 99% of the variance in the data. We can observe this in the below plot.


```{r, include=TRUE}
#Let us plot the cumulation of variance using the sd
pr.var <- (pr.out$sdev^2)
pve <- pr.var/sum(pr.var)
plot(cumsum(pve), xlab="Principal Components", ylab ="Cumulative Proportion of Variance Explained",ylim=c(0,1),type="b")
```
Let us perform regression analysis based on the Principal Component Analysis which is Principal Component Regression(PCR). We can validate the R2 based on the number of componenets used.

```{r pcrmodel, include=TRUE}
#Again we want to scale our data and "CV" stands for cross validation, which is defaulted to RMSE.
pcrfit=pcr(Official.Time~X5K + X10K + X15K + X20K + Half + X25K + X30K + X35K + X40K, data = splits, scale = TRUE,validation ="CV")
summary(pcrfit)
validationplot(pcrfit,val.type="RMSEP")
validationplot(pcrfit,val.type="MSEP")
validationplot(pcrfit,val.type = "R2")
predplot(pcrfit, ncomp = 2)
coefplot(pcrfit)
```

In the Principal Component Regression, 1 component cover 97% of the variance in the split times and 92% of the variance in the Official time where as 2 components cover 99% of the variance in the split times and 98% of the variance in the Official time. 
R2 value is close to .95 if we consider 1 component and it is close to .98 if we considered 2 compoenents i.e 2 principal components cover 98% of the variance in the Official time.

