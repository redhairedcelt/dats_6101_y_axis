# Ok so lets read in the file.  I'm using the standard R 'read.csv'
# rather than the readr 'read_csv' since that package defaults all time 
# to a specific time of day.  I found the 'read.csv' easier to use.
bm_2017 <- read.csv('dats_6101_y_axis/marathon_results_2017.csv')

bm_2017$Official.Time <- as.character(bm_2017$Official.Time)
bm_2017$Official.Time.Min <- period_to_seconds(hms(bm_2017$Official.Time))/60

# I want to run some analysis on the different states that runners come from.
# I'll look at the states with the three most runners, and then run an ANOVA
# comparison on the means of their times.  To do this, I only need the fields
# 'Official.Time.Min' and 'State' so I'll subset down to save some memory.

times_states <- bm_2017[,c('State', 'Official.Time.Min')]

top_states <- count(times_states, State, sort=T)
head(top_states)
top3_states <- c('MA', 'CA', 'NY')

#So the top states are MA, CA, and NY.  Let's make individual dataframes.
runners_ma <- subset(times_states, State=='MA')
runners_ca <- subset(times_states, State=='CA')
runners_ny <- subset(times_states, State=='NY')

mean_ma <- mean(runners_ma$Official.Time.Min)
mean_ca <- mean(runners_ca$Official.Time.Min)
mean_ny <- mean(runners_ny$Official.Time.Min)

# This seems messier for plotting...  What if we keep them all as one dataframe?
runners_top3_states <- subset(times_states, State %in% c('MA', 'CA', 'NY'))
# There are still 69 factors, so we are going to make the states column charachters,
# then change them back to a factor.
runners_top3_states$State <- as.character(runners_top3_states$State)
runners_top3_states$State <- as.factor(runners_top3_states$State)
str(runners_top3_states)

# Ok we are getting there! 
plot(Official.Time.Min ~ State, data=runners_top3_states)

# Wow, the plot DEFINITELY shows that MA runners are slower than those from CA and NY.
# This supports the theory that runners in MA, who are closer to Boston, may not be
# as elite as runners who travel from across the country.

# Now lets look at the ANOVA ANALYSIS!
anovaRes = aov(Official.Time.Min ~ State, data=runners_top3_states)
anovaRes
names(anovaRes)
summary(anovaRes)

tukeySmokeAoV <- TukeyHSD(anovaRes)
tukeySmokeAoV

# So there is an incredibly small p-value, suggesting we reject the null hypothesis that 
# there is no difference between the means.  However, for CA and NY the means are very
# close yet the p value is .03.  Perhaps its all the outliers in CA and NY.  What if we 
# conduct a t-test on those two alone?

ttest = t.test(runners_ca$Official.Time.Min, runners_ny$Official.Time.Min)
ttest
# So even as part of a two-way t-test, the p-value is incredibly low at .004555,
# even though the difference in mean between CA and NY runners is only three minutes!

# What if we remove outliers?  I'll use a function I wrote to do this.
remove_outliers <- function(df, column) {
  iqr = IQR(column, na.rm = TRUE)
  first_qu = quantile(column, .25, na.rm = TRUE)
  third_qu = quantile(column, .75, na.rm = TRUE)
  high_outlier_limit = third_qu + (iqr*1.5)
  low_outlier_limit = first_qu - (iqr*1.5)
  cleaned <- subset(df, (column > low_outlier_limit & column < high_outlier_limit))
  return(cleaned)
}

ca_cleaned <- remove_outliers(runners_ca, runners_ca$Official.Time.Min)
ny_cleaned <- remove_outliers(runners_ny, runners_ny$Official.Time.Min)

ttest = t.test(ca_cleaned$Official.Time.Min, ny_cleaned$Official.Time.Min)
ttest
cleaned <- rbind(ca_cleaned, ny_cleaned)
cleaned$State <- as.character(cleaned$State)
cleaned$State <- as.factor(cleaned$State)
plot(Official.Time.Min ~ State, data=cleaned)
# EVEN WORSE!  p-value of .002145

# We should have checked this first.  Is this a normal distribution?
hist(runners_ca$Official.Time.Min)
qqnorm(runners_ca$Official.Time.Min)
qqline(runners_ca$Official.Time.Min)

hist(runners_ny$Official.Time.Min)
qqnorm(runners_ny$Official.Time.Min)
qqline(runners_ny$Official.Time.Min)
# So, they arent normally distributed.

# What if we sample:
nrow(runners_ca)
nrow(runners_ny)

sample_ca <- runners_ca[ sample(nrow(runners_ca),50), ]
sample_ny <- runners_ny[ sample(nrow(runners_ny),50), ]

hist(sample_ca$Official.Time.Min)
qqnorm(sample_ca$Official.Time.Min)
qqline(sample_ca$Official.Time.Min)

hist(sample_ny$Official.Time.Min)
qqnorm(sample_ny$Official.Time.Min)
qqline(sample_ny$Official.Time.Min)
# Ok, looking more normal, but still skewed.

ttest = t.test(sample_ca$Official.Time.Min, sample_ny$Official.Time.Min)
ttest

samples <- rbind(sample_ca, sample_ny)
samples$State <- as.character(samples$State)
samples$State <- as.factor(samples$State)
plot(Official.Time.Min ~ State, data=samples)

# This is really interesting.  When we test for normality in the sample, we get 
# something closer to a normal distribution (as expected).  If you run lines 63 to 72
# repeatedly, you can  and see how the random samples affect p. But yes, with 
# sampling 50 observations, we finally see that most of the time, the p value 
# is usually quite high.  Need to do more research on how to treat these if
# they come from non-normal distributions.





  What if we remove outliers?  

I'll use a function I wrote to do this. (Lets not include this)

```{r}
remove_outliers <- function(df, column) {
iqr = IQR(column, na.rm = TRUE)
first_qu = quantile(column, .25, na.rm = TRUE)
third_qu = quantile(column, .75, na.rm = TRUE)
high_outlier_limit = third_qu + (iqr*1.5)
low_outlier_limit = first_qu - (iqr*1.5)
cleaned <- subset(df, (column > low_outlier_limit & column < high_outlier_limit))
return(cleaned)
}

ca_cleaned <- remove_outliers(runners_ca, runners_ca$Official.Time.Min)
ny_cleaned <- remove_outliers(runners_ny, runners_ny$Official.Time.Min)

ttest = t.test(ca_cleaned$Official.Time.Min, ny_cleaned$Official.Time.Min)
ttest
cleaned <- rbind(ca_cleaned, ny_cleaned)
cleaned$State <- as.character(cleaned$State)
cleaned$State <- as.factor(cleaned$State)
plot(Official.Time.Min ~ State, data=cleaned)
```
